import crypto from 'crypto';

export default function handler(req, res) {

    // Defining key
    const key = crypto.scryptSync(req.body.randomString, req.body.sign, 32); //hash and signature

    // Defininf iv
    const iv = Buffer.alloc(16, process.env.NEXT_PUBLIC_INITIALIZATION_VECTOR);
    
    // Creating cipher
    const cipher = crypto.createCipheriv(process.env.NEXT_PUBLIC_ENCRYPTION_ALGORITHM, key, iv);

    let encrypted = cipher.update(JSON.stringify(req.body.data), 'utf8', 'hex');
    encrypted += cipher.final('hex');
    console.log(encrypted);

    res.status(200).json({ encryptedData: encrypted })
}
  