import crypto from 'crypto';

export default function handler(req, res) {

    // Defining key
    const key = crypto.scryptSync(req.body.randomString, req.body.sign, 32); //hash and signature

    // Defining iv
    const iv = Buffer.alloc(16, process.env.NEXT_PUBLIC_INITIALIZATION_VECTOR);

    // Creating decipher
    const decipher = crypto.createDecipheriv(process.env.NEXT_PUBLIC_ENCRYPTION_ALGORITHM, key, iv);

    let encryptedText = Buffer.from(req.body.data, 'hex');

    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    console.log(decrypted.toString());

    res.status(200).json({ decryptedData: decrypted.toString() })
}
  