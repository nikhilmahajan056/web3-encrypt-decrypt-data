import Head from 'next/head'
import { useState, useEffect } from 'react';
import * as Web3 from 'web3';
import styles from '../styles/Home.module.css'
const abi = require('ethereumjs-abi');
import { randomBytes } from 'crypto';

export default function Home() {

  const [accountAddress, setAccountAddress] = useState("");
  const [encryptUserAddress, setEncryptUserAddress] = useState("");
  const [decryptUserAddress, setDecryptUserAddress] = useState("");
  const [data, setData] = useState("");
  const [encryptedData, setEncryptedData] = useState("");
  const [randomHash, setRandomHash] = useState("");
  const [signature, setSignature] = useState("");
  const [responseEncryptedData, setResponseEncryptedData] = useState("");
  const [responseDecryptedData, setResponseDecryptedData] = useState("");
  const [error, setError] = useState("");

  useEffect(() => {
    console.log("accountAddress is:", accountAddress);
    connectWallet();
  }, [accountAddress]);

  const monitorWallet = async () => {
    try {
      // Add listeners start
      setError("");
      window.ethereum.on("accountsChanged", (accounts) => {
          accounts[0] === undefined ? window.location.reload() : setAccountAddress(accounts[0]);
      });

      window.ethereum.on("connect", () => {
        window.location.reload();
      });

      window.ethereum.on("disconnect", () => {
        window.location.reload();
      });
    } catch (exc) {
      setError('An unexpected error occurred:'+exc.message);
      console.log(exc.message);
    }
  };

  const connectWallet = async () => {
    try {
      const accounts = await window.ethereum.request({
        method: "eth_requestAccounts",
      });
      setAccountAddress(accounts[0]);
      
      monitorWallet();
    } catch (exc) {
      setError('An unexpected error occurred:'+exc.message);
      console.log(exc.message);
    }
  };

  const generateSignature = async(userAddress, randomString) => {
    const web3 = new Web3(window.ethereum);
    const msg = `0x${abi
      .soliditySHA3(['address', 'string'], [userAddress, randomString])
      .toString('hex')}`;
    const msgHash = web3.eth.accounts.hashMessage(msg);
    console.log("hash is:", msgHash);
    const sig = await web3.eth.sign(msgHash, accountAddress);
    return sig;
  };

  const encrypt = async () => {
    try {
      var randomString = randomBytes(32).toString('hex');
      console.log("randomString", randomString);
      const sign = await generateSignature(encryptUserAddress.toLowerCase(), randomString);
      console.log("signature is:", sign);
  
      const body = {
        randomString: randomString,
        sign: sign,
        encryptUserAddress: encryptUserAddress.toLowerCase(),
        data: JSON.parse(data)
      };
  
      const res = await fetch('/api/encrypt_data', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body),
      });
      if (res.status === 200) {
        const res_json = await res.json();
        setResponseEncryptedData(res_json.encryptedData);
      } else {
        throw new Error(await res.text())
      }
    } catch (exc) {
      setError('An unexpected error occurred:'+exc.message);
      console.log(exc.message);
    }
  };

  const recoverSigner = async(randomString, signature) => {
    const web3 = new Web3(window.ethereum);
    const msg = `0x${abi
      .soliditySHA3(['address', 'string'], [accountAddress, randomString])
      .toString('hex')}`;
    const recoveredAddress = await web3.eth.accounts.recover(msg, signature);
    return recoveredAddress;
  };

  const decrypt = async () => {
    try {
      const body = {
        randomString: randomHash,
        sign: signature,
        decryptUserAddress: decryptUserAddress.toLowerCase(),
        data: encryptedData
      };

      const recoveredUser = await recoverSigner(randomHash, signature);
      if (recoveredUser.toLowerCase() === decryptUserAddress.toLowerCase()) {
        const res = await fetch('/api/decrypt_data', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(body),
        });
        if (res.status === 200) {
          const res_json = await res.json();
          setResponseDecryptedData(JSON.stringify(res_json.decryptedData));
        } else {
          throw new Error(await res.text())
        }
      } else {
        throw new Error("User address or account address is invalid!")
      }
    } catch (exc) {
      setError('An unexpected error occurred:'+exc.message);
      console.log(exc.message);
    }
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>Data Encryption & Decryption</title>
        <meta name="description" content="encrypt and decrypt data here" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Data Encryption & Decryption
        </h1>

        {
          accountAddress === "" ? <button className="connect" onClick={connectWallet}>Connect to Metamask!</button>
          : (
            <div className={styles.grid}>
              <div className={styles.card}>
                <h2>Encryption &rarr;</h2>
                <label>Enter User address:</label>
                <input className={styles.text} onChange={(e) => setEncryptUserAddress(e.target.value)}></input>
                <br/>
                <label>Add your data below:</label>
                <textarea className={styles.textarea} onChange={(e) => setData(e.target.value)}></textarea>
                <br/>
                <button onClick={encrypt}>Encrypt data</button>
                <br/>
                {
                  error === "" ? (
                    <>
                      <label>Encrypted data:</label>
                      <p>{responseEncryptedData}</p>
                    </>
                  ) : (
                    <>
                      <label>Error:</label>
                      <p>{error}</p>
                    </>
                  )
                }
              </div>

              <div className={styles.card}>
                <h2>Decryption &rarr;</h2>
                <label>Enter User address:</label>
                <input className={styles.text} onChange={(e) => setDecryptUserAddress(e.target.value)}></input>
                <br/>
                <label>Enter hash(Random string):</label>
                <input className={styles.text} onChange={(e) => setRandomHash(e.target.value)}></input>
                <br/>
                <label>Enter signature:</label>
                <input className={styles.text} onChange={(e) => setSignature(e.target.value)}></input>
                <br/>
                <label>Enter your encrypted data below:</label>
                <textarea className={styles.textarea} onChange={(e) => setEncryptedData(e.target.value)}></textarea>
                <br/>
                <button onClick={decrypt}>Decrypt data</button>
                <br/>
                {
                  error === "" ? (
                    <>
                      <label>Decrypted data:</label>
                      <p>{responseDecryptedData}</p>
                    </>
                  ) : (
                    <>
                      <label>Error:</label>
                      <p>{error}</p>
                    </>
                  )
                }
              </div>
            </div>
          )
        }
      </main>
    </div>
  )
}