# Web3 Data Encryption Example

The example features how to protect the user data and only allow access to specific user.

The example shows how to connect to Metamask wallet; and to create a signature specifically to the user who wants to access the data.

The user(who wants to access the data) must then connect to their metamask wallet and must provide the required details in the decryption tab.

The decrypted data will be visible only when the correct user is connected to the website with valid signature and random string characters.

## How to use

Clone the repository and execute below command:

```bash
cd web3-encrypt-decrypt-data
```

Rename .env.example file to .env and add all the required variables

After adding all the required variables in the .env file execute below commands in the sequential manner:

```bash
npm install

npm run dev
```